# phonecallrecorder

Prechter Lab Demo

# How to import into Android Studio

1. Download the project for bitbucket onto local machine.
2. Start Android Studio.
3. From the modal box choose "Open an existing Android Studio Project".
4. Then run.

# How to test
I tested by running the project on my android samsung phone of 4+ years ago. With the app running from within Android Studio,
I placed a call to my android phone and answered it. After a few seconds I hung up. Each recording is stored on the device, so subsequent
restarts (I am sorry, run out of time) of the app show the previous recordings on the list view.
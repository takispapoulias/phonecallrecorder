package phonecallrecorder;

import android.content.Context;
import android.content.Intent;
import android.content.BroadcastReceiver;
import android.util.Log;

public class RecordingServiceRestartBroadcastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i(RecordingServiceRestartBroadcastReceiver.class.getSimpleName(), "Recording Service Stopping...");
        context.startService(new Intent(context, RecordingService.class));
    }
}

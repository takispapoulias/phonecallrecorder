package phonecallrecorder;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.io.File;

/**
 * Adapter responsible for managing the recordings
 */
public class RecordingAdapter extends RecyclerView.Adapter<RecordingAdapter.RecordingsViewHolder> {

    private File[] files;

    /**
     * Holds textView in memory for quick retrieval
     */
    public static class RecordingsViewHolder extends RecyclerView.ViewHolder {
        public TextView textView;
        public RecordingsViewHolder(View v) {
            super(v);
            textView = v.findViewById(R.id.textview_row_item);;
        }
    }

    public RecordingAdapter(File[] files){
        this.files=files;
    }

    @Override
    public RecordingsViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_row, parent, false);
        return new RecordingsViewHolder(view);
    }

    /**
     *  Replace the contents of a view (invoked by the layout manager)
     *
     */
    @Override
    public void onBindViewHolder(RecordingsViewHolder holder, int position) {
        holder.textView.setText(files[position].getName());
    }


    /**
     *   // Return the size of your dataset (invoked by the layout manager)
     * @return The size of the dateset
     */
    @Override
    public int getItemCount() {
        return files.length;
    }
}
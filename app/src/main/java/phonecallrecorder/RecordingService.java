package phonecallrecorder;

import android.app.IntentService;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.IBinder;
import android.telephony.TelephonyManager;
import android.util.Log;

import java.io.File;
import java.io.IOException;

public class RecordingService extends IntentService {

    private static final String TAG = "RecordingService";

    private static final String FILE_CREATE_ERROR = "Unable To Create Audio File";
    private boolean recordStarted = false;

    private static final String ACTION_PHONE_STATE = "android.intent.action.PHONE_STATE";
    private static final String ACTION_NEW_OUTGOING_CALL = "android.intent.action.NEW_OUTGOING_CALL";

    private CallBroadcastReceiver callBroadcastReceiver;

    public RecordingService() {
        super("PrechterLabService");
    }

    @Override
    public IBinder onBind(Intent arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "destroy");
       // this.unregisterReceiver(this.callBroadcastReceiver);
        super.onDestroy();
    }

    /**
     * This method is called in the background thread.
     * Uncommeting the code below causes a crash. For now this logic has been moved to
     * onStartCommand(). Demo daemon strikes again!
     * @param intent
     */
    @Override
    protected void onHandleIntent(Intent intent) {
       /* final IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_NEW_OUTGOING_CALL);
        filter.addAction(ACTION_PHONE_STATE);
        this.callBroadcastReceiver = new CallBroadcastReceiver();
        this.registerReceiver(this.callBroadcastReceiver, filter);
        Log.d(TAG,"onHandleIntent");*/
    }

    /**
     *
     * @param intent
     * @param flags
     * @param startId
     * @return START_STICKY - Tell the OS to try not to call onDestroy if resources are low.
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        final IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_NEW_OUTGOING_CALL);
        filter.addAction(ACTION_PHONE_STATE);
        this.callBroadcastReceiver = new CallBroadcastReceiver();
        this.registerReceiver(this.callBroadcastReceiver, filter);
        Log.d(TAG,"onHandleIntent");
        return START_STICKY;
    }

    /**
     * Manages the phone call.
     */
    public class CallBroadcastReceiver extends BroadcastReceiver {
        Bundle bundle;
        String state;
        String incomingCall, outgoingCall;
        public boolean isRinging = false;
        MediaRecorder recorder;
        File audioFile;

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG,"onReceive");
            if (intent.getAction().equals(ACTION_PHONE_STATE)) {
                if ((bundle = intent.getExtras()) != null) {
                    state = bundle.getString(TelephonyManager.EXTRA_STATE);
                    if (state.equals(TelephonyManager.EXTRA_STATE_RINGING)) {
                        incomingCall = bundle.getString(TelephonyManager.EXTRA_INCOMING_NUMBER);
                        isRinging = true;
                        Log.d(TAG,"INCOMING CALL -> "+ incomingCall);
                    } else if (state.equals(TelephonyManager.EXTRA_STATE_OFFHOOK)) {
                        if (isRinging == true) {
                            Log.d(TAG,"Call Answered");
                            try {
                                audioFile = Recordings.createAmrFile();
                            } catch (IOException e) {
                                Log.e(TAG,FILE_CREATE_ERROR);
                                e.printStackTrace();
                            }
                            recorder = new MediaRecorder();

                            recorder.setAudioSource(MediaRecorder.AudioSource.VOICE_COMMUNICATION);
                            recorder.setOutputFormat(MediaRecorder.OutputFormat.AMR_NB);
                            recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
                            if(audioFile!=null) {//Means exception was thrown above.
                                recorder.setOutputFile(audioFile.getAbsolutePath());
                            }
                            try {
                                recorder.prepare();//In real life better error handling
                            } catch (IllegalStateException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            startRecording();
                        }
                    } else if (state.equals(TelephonyManager.EXTRA_STATE_IDLE)) {
                        isRinging = false;
                        Log.d(TAG,"REJECTED / DISCONNECTED");
                        if (recordStarted) {
                            stopRecording();
                        }
                    }
                }
            } else if (intent.getAction().equals(ACTION_NEW_OUTGOING_CALL)) {
                if ((bundle = intent.getExtras()) != null) {
                    outgoingCall = intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER);
                    Log.d(TAG,"OUTGOING CALL -> "+ outgoingCall);
                }
            }
        }

        /**
         * Helper method to commence recording
         */
        private void startRecording(){
            recorder.start();
            recordStarted = true;
        }

        /**
         * Helper method to stop recording
         */
        private void stopRecording(){
            recorder.stop();
            recordStarted = false;
        }

    }

}
package phonecallrecorder;

import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Helper class to manage the storage area where a recording will be stored.
 * Provides convenience functions C.R.U.D operations.
 */
public class Recordings {

    /**
     * The name of the root directory.
     */
    private final static String ROOT = "/PrechterLab";

    /**
     * The prefix string of every filename.
     */
    private final static String FILENAME_PREFIX = "Recorded-";

    /**
     * The extension of the file
     */
    private final static String FILENAME_EXT = ".amr";

    /**
     * The pattern with which the Date() object will be formatted, and become part of the filename.
     */
    private final static String DATE_FORMAT_PATTERN = "dd-MM-yyyy hh-mm-ss";

    /**
     * The file object representing the root directory.
     */
    private static File rootDir;

    /**
     * static block that creates the creates the rootDir and checks if path exists. If path does not
     * exist it calls mkdirs().
     */
    static{
        rootDir = new File(Environment.getExternalStorageDirectory(), ROOT);
        if (!rootDir.exists()) {
            rootDir.mkdirs();
        }
    }

    /**
     *
     * Creates a file under the root directory with a name that includes the current date
     * and the FILENAME_PREFIX i.e. FILENAME_PREFIX-@date. The date is formatted as dictated
     * by DATE_FORMAT_PATTERN. The extension of the file is AMR.
     *
     * @return File - An AMR audio File.
     * @throws IOException - If the audio file was not created. May be a sign of not enough space on
     * storage device.
     */
    public final static File createAmrFile() throws IOException{

        File audioFile=null;
        String date = new SimpleDateFormat(DATE_FORMAT_PATTERN).format(new Date());
        String audioFileName = FILENAME_PREFIX + date + FILENAME_EXT;
        audioFile = new File(rootDir,audioFileName);
        audioFile.createNewFile();
        return audioFile;
    }

    /**
     * Helper method for listing all files with conversation recordings.
     * @return A File array with all recordings under the root dir.
     */
    public final static File[] listFiles()
    {
        File[] files= rootDir.listFiles();
        return files!=null ? files : new File[0];
    }
}

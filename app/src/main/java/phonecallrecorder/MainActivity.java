package phonecallrecorder;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    /**
     * Tag for use with Log()
     */
    private static final String TAG = "MainActivity";

    /**
     * Codes for permissions
     */
    private static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 0;
    /**
     * Code for activity result
     */
    private static final int REQUEST_CODE = 1;

    Intent serviceIntent;
    private RecordingService service;

    /**
     * Holder object for recyclerView
     */
    private RecyclerViewWithEmptyView recyclerView;

    /**
     * Holder object for adapter
     */
    private RecyclerView.Adapter adapter;

    /**
     * Holder object for recyclerview layoutManager
     */
    private RecyclerView.LayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // configure recycler view
        recyclerView = findViewById(R.id.recordings_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        //help out with the rendering of the view
        recyclerView.setHasFixedSize(true);

        // use a linear layout manager
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        //Get the previous recordings and add adapter
        adapter = new RecordingAdapter(Recordings.listFiles());
        recyclerView.setAdapter(adapter);

        // Fetch default view from the layout and set it on
        // the new recycler view
        View defaultView = findViewById(R.id.recordings_default_recycler_view);
        recyclerView.setDefaultView(defaultView);

        //Check permissions and carry on the normal flow if all is well
        if(checkAndRequestPermissions()) {
            startService();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (REQUEST_CODE == requestCode) {
            startService();
        }
    }

    /**
     * Check if service is already running
     * @param serviceClass
     * @return
     */
    private boolean isServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i ("Service Running =" , true+"");
                return true;
            }
        }
        Log.i ("Service Running =", false+"");
        return false;
    }

    /**
     * Starts the service is service is not already running
     */
    private void startService(){
        service = new RecordingService();
        serviceIntent = new Intent(this, service.getClass());
        if (!isServiceRunning(service.getClass())) {
            startService(serviceIntent);
        }
    }

    @Override
    protected void onDestroy() {
        stopService(serviceIntent);
        Log.i(TAG, "onDestroy!");
        super.onDestroy();
    }

    /**
     * Check permissions and request approval from user.
     * @return
     */
    private  boolean checkAndRequestPermissions() {
        int recordAudioPermission = ContextCompat.checkSelfPermission(this,
                Manifest.permission.RECORD_AUDIO);
        int writeExternalStoragePermission = ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int readPhoneStatePermission = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (readPhoneStatePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_PHONE_STATE);
        }
        if (recordAudioPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.RECORD_AUDIO);
        }
        if (writeExternalStoragePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }


    /**
     * When the user is done assigning preference to permissions this method is called to decide
     * what to do next.
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_ID_MULTIPLE_PERMISSIONS: {

                Map<String, Integer> perms = new HashMap<>();
                perms.put(Manifest.permission.RECORD_AUDIO, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.READ_PHONE_STATE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                // Fill with actual results from user
                if (grantResults.length > 0) {
                    for (int i = 0; i < permissions.length; i++)
                        perms.put(permissions[i], grantResults[i]);
                    // Check permissions
                    if (perms.get(Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        Log.d(TAG, "services permission granted");
                        startService();
                    } else {
                        Log.d(TAG, "Some permissions are not granted should ask again ");
                        //TODO: Add UI asking the permissions again one last time...
                    }
                }
            }
        }

    }
}
